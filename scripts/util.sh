#!/usr/bin/env bash
BOLD='\033[1m'
DIM='\033[2m'
ULINE='\033[4m'

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
MAGENTA='\033[0;35m'
CYAN='\033[0;36m'
WHITE='\033[0;30m'
BLACK='\033[0;37m'
NO_COLOR='\033[0m'

function line(){
    printf "${YELLOW}----------------------------------------------${NO_COLOR}\n"
}


