#!/usr/bin/env bash
MY_DIR="$(dirname "$0")"
echo $MY_DIR
. ${MY_DIR}/scripts/util.sh

VERBOSE=$1
MODE=$2
BREWFILE="${MY_DIR}/scripts/bundles/Brewfile-$MODE"
if [[ ${VERBOSE} -eq 1 ]];then
  echo "VERBOSE: $VERBOSE MODE: $MODE"
  echo "BREWFILE: $BREWFILE"
fi

# All these applications are independent, so if one
# fails to install, don't stop.
set +e

line
echo -e "${YELLOW}Installing from ${BREWFILE}${NO_COLOR}"

if [[ ${VERBOSE} -eq 1 ]]; then
    line
    echo -e "${ORANGE} Installing bundle ${BREWFILE}"
    
    brew bundle install --file=${BREWFILE}

    echo "Completed all brew installations ${NO_COLOR}"
else
    brew bundle install --file=${BREWFILE} > /dev/null
fi
line

if [[ $? -ne 0 ]]; then
    line
    echo "An error happened. Run the script with -v to see complete installation logs"
    line
    echo "Current list "
    brew list
fi

set -e
