#!/usr/bin/env bash
MY_DIR="$(dirname "$0")"

. ${MY_DIR}/scripts/util.sh

REINSTALL=$1
FIRST_TIME=0

if hash brew 2>/dev/null; then
  echo "Homebrew is already installed!"
else
  FIRST_TIME=1
  echo "Installing Homebrew..."
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

echo
echo "Ensuring your Homebrew directory is writable..."
sudo chown -R $(whoami) /usr/local/bin

echo
echo "Adding Homebrew's sbin to your PATH..."
echo 'export PATH="/usr/local/sbin:$PATH"' >> ~/.bash_profile

if [[ ${FIRST_TIME} -eq 0 && ${REINSTALL} -eq 0 ]]; then
  line
  echo "Cleaning up your Homebrew installation..."
  brew cleanup

  echo
  echo "Ensuring you have the latest Homebrew..."
  brew update-reset
  brew update

  echo
  echo "Upgrading existing brews..."
  brew upgrade
fi

#Clean up all installations 
if [[ ${FIRST_TIME} -eq 0 && ${REINSTALL} -eq 1 ]]; then
echo
echo "Starting cleanup.."
  ${MY_DIR}/scripts/cleanup.sh
  if [[ $? -eq 0 ]]; then
    echo
    echo -e "${GREEN}Cleanup completed successfully..${NO_COLOR}"
    line
  fi
fi

