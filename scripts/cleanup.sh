#!/bin/bash
MY_DIR="$(dirname "$0")"
set +e 
. ${MY_DIR}/util.sh
echo -e "In dir ${MY_DIR}"

pushd ${MY_DIR}/bundles

# Collect whitelist for brew bundle cleanup
cat Brewfile-common  > Brewfile
cat Brewfile-designer  >> Brewfile
cat Brewfile-developer  >> Brewfile
cat Brewfile-java  >> Brewfile
cat Brewfile-node  >> Brewfile

# Cleanup 
brew bundle cleanup --force
rm Brewfile

popd

brew cleanup
brew cask cleanup
