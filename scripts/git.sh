#!/usr/bin/env bash
MY_DIR="$(dirname "$0")"

. ${MY_DIR}/scripts/util.sh
echo $MY_DIR

line
echo "Configuring Git and associated tools"
line


echo
echo "Enabling git-duet global author configuration"
echo "export GIT_DUET_GLOBAL=true" >> ~/.bash_profile
echo "export GIT_DUET_SET_GIT_USER_CONFIG=1" >> ~/.bash_profile

echo
echo "Setting global Git configurations"
git config --global core.editor /usr/bin/vim
git config --global transfer.fsckobjects true

source ${MY_DIR}/scripts/configurations.sh
source ${MY_DIR}/scripts/configuration-bash.sh
source ${MY_DIR}/scripts/git-aliases.sh
