#!/usr/bin/env bash
MY_DIR="$(dirname "$0")"

. ${MY_DIR}/scripts/util.sh

VERBOSE=$1

line
source ${MY_DIR}/scripts/bundle-install.sh $VERBOSE "java"

echo -e "${RED}Installing Java Development tools${NO_COLOR}"

echo "export JAVA_HOME=`/usr/libexec/java_home -v 1.11`" >> ~/.bash_profile

source ${MY_DIR}/scripts/download-pivotal-ide-prefs.sh

pushd ~/workspace/pivotal_ide_prefs/cli
./bin/ide_prefs install --ide=intellij	
popd

#install intelliJ plugins
mkdir -p plugin-temp
declare -a plugins=(
	'http://plugins.jetbrains.com/files/9442/53971/vuejs-183.5429.1.zip?updateId=53971&pluginId=9442'
	'http://plugins.jetbrains.com/files/6317/51518/lombok-plugin-0.22-183.2153.8.zip?updateId=51518&pluginId=6317'
    'http://plugins.jetbrains.com/files/7287/53373/js-karma-183.5153.1.zip?updateId=53373&pluginId=7287'
	'http://plugins.jetbrains.com/files/6954/45423/kotlin-plugin-1.2.41-release-IJ2018.1-1.zip?updateId=45423&pluginId=6954',
	'http://plugins.jetbrains.com/files/7345/39861/Presentation_Assistant-1.0.1.zip?updateId=39861&pluginId=7345'
)
for PLUGIN in "${plugins[@]}"
do 
	wget -O plugin.zip $PLUGIN 
	unzip -o plugin.zip -d plugin-temp
	rm -rf plugin.zip
done
cp -r plugin-temp/ ~/Library/Application\ Support/IntelliJIdea2018.3
rm -rf plugin-temp
