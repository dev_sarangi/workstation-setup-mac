#!/usr/bin/env bash
MY_DIR="$(dirname "$0")"

. ${MY_DIR}/scripts/util.sh

VERBOSE=$1

source ${MY_DIR}/scripts/bundle-install.sh $VERBOSE "developer"
source ${MY_DIR}/scripts/bundle-install.sh $VERBOSE "java"

source ${MY_DIR}/scripts/configuration-osx.sh
source ${MY_DIR}/scripts/configuration-bash.sh
source ${MY_DIR}/scripts/configurations.sh

source ${MY_DIR}/scripts/opt-ins/java.sh
