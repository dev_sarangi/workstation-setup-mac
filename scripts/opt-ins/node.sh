#!/usr/bin/env bash
MY_DIR="$(dirname "$0")"

. ${MY_DIR}/scripts/util.sh

VERBOSE=$1

line
echo -e "${GREEN}Installing Node and related tools${NO_COLOR}"

source ${MY_DIR}/scripts/bundle-install.sh $VERBOSE "node"

echo
echo -e "${GREEN}Installing global NodeJS Packages${NO_COLOR}"
line

npm install --global @angular/cli
