# Github Key Registration

## Follow this guide to register your station's SSH key with GitHub.

### Generating a key
Navigate to your ssh keys on your USB drive. You can copy the public key to the clipboard with:
```
cat .ssh/id_rsa.pub | pbcopy
```

### Registering the key with Github
* Navigate to github.ford.com and login with your credentials.
* Go to 'Your Profile' and click 'Edit Profile.'
* Go to SSH and GPG keys.
* Click 'New SSH Key'.
* Name your USB key in the Title. eg. 'Ford Labs USB key - (your name here)'
* Paste your key in the section for the key. (This was copied with the last command in the terminal - if you've copied anything since, you may need to re-run that command)
* Click 'Add SSH Key.'
* Profit

Note: You may need to get in contact with an admin of the repository/repositories you're going to be working on to get added.
