# Workstation Setup scripts for Macs

This is a simplified version of the Pivotal workstation setup scripts. (Also used by FordLabs)
Original Pivotal setup Github available [here]https://github.com/pivotal/workstation-setup

## Prerequisites

Install xcode

```sh
xcode-select --install
```

## Usage

- You must be an **administrator** to run the script.
- You must be on the public wifi in order for a successful install.
- In a new Terminal Window navigate to the root of the project
- Run `./setup.sh -h` to see options
- Typically for a developer, run `./setup.sh developer node`

Open up `Terminal.app` and run the following commands:

```sh
mkdir -p ~/workspace
cd ~/workspace
git clone <Clone your repo>
cd workstation-setup
./setup.sh -h
```

## Notes

- This script takes a while. Please be patient.
- Upon starting, this script will install Homebrew if it did not detect homebrew
- Uses Homebrew's `brew bundle` plugin to install dependencies. [Brew bundle Documentation](https://github.com/Homebrew/homebrew-bundle)
- A new window for `shiftIt` will open while the script is running. *Please ignore it and let it remain open.*
- `Intellij` is downloaded here as default with `pivotal-ide-prefs` project applied. This gives greater flexibility and advanced shortcuts to ease developer productivity. `pivotal-ide-prefs` is a separate project maintained by pivotal for number of IDEs. [Link to documentation](https://github.com/pivotal-legacy/pivotal_ide_prefs). 
- This script sets up git aliases and stores them in the bash_script. You can see all the git aliases set in the ~/.bash_profile. To learn more about git aliases, [read the documentation here](https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases)

### Engineering Machine

You're setting up an engineering machine! Here's the script to run:

```sh
./setup.sh developer node
```

## Customizing

If you'd like to customize this project for a project's use:

- Fork the project
- Edit the shells scripts to your liking
