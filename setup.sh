#!/usr/bin/env bash
. ./scripts/util.sh

usage() {
    echo -e "  ${YELLOW}Usage: ${NO_COLOR}./setup.sh ${MAGENTA}[-hvrs] ${CYAN}[dev]${NO_COLOR}"
    echo -e "                              ${MAGENTA}-v: Show bundle install output.${NO_COLOR}"
    echo -e "                              ${MAGENTA}-r: Uninstall and then reinstall bundle'.${NO_COLOR}"
    echo -e "                              ${MAGENTA}-h: Show usage.${NO_COLOR}"
    echo -e "                              ${MAGENTA}-s: Skip common and run script'.${NO_COLOR}"
    echo -e "       ${CYAN}[dev]: Use one or more of these options to install libraries'.${NO_COLOR}"
    echo -e "                           ${CYAN} Note: \"dev\" will setup bash configurations, java tools and git.${NO_COLOR}"
    exit 1
}

runScript() {
  local var=$1
  if [ -f ${MY_DIR}/scripts/opt-ins/${var}.sh ]; then
        source ${MY_DIR}/scripts/opt-ins/${var}.sh $VERBOSE
    else
       echo "Warning: $var does not appear to be a valid argument. File $FILE does not exist."
    fi
}

VERBOSE=0
REINSTALL=0
SOLO=""
while getopts "vhrks:" opt; do
  case $opt in
    v) VERBOSE=1 ;;
    r) REINSTALL=1 ;;
    h) usage ;;
    s) SOLO=$2 ;;
    k) ALT_KEY=${OPTARG};;
    \?) echo "Invalid option: -$OPTARG" >&2; usage; exit 2 ;;
  esac
done
shift $((OPTIND -1))

# Fail immediately if any errors occur
set -e

echo 
echo -e "${YELLOW}Run setup.sh -h to see usage options ${NO_COLOR}"

echo -e "${NO_COLOR}"

MY_DIR="$(dirname "$0")"
if [[ $VERBOSE -eq 1 ]]; then
  echo -e "Running in ${MY_DIR}"
fi


echo -e "${CYAN}Caching password..."
sudo -K
sudo true;

# Homebrew needs to be set up first
echo -e"${GREEN}"
read -p "Re-install Homebrew?" brew_reinstall
echo -e"${NO_COLOR}"

if [[ "${brew_reinstall}" == "y" ]];then
  source ${MY_DIR}/scripts/homebrew.sh $REINSTALL
  sudo chown -R $(whoami) $(brew --prefix)
  sudo chown -R $(whoami) /Library/Caches/Homebrew
else
  # Sanity Check
  # Check brew for potential errors
  brew doctor
fi

line
if [[ -z "$SOLO" ]]; then
  
  source ${MY_DIR}/scripts/bundle-install.sh $VERBOSE "common"
  source ${MY_DIR}/scripts/git.sh
else
  echo -e "Running SOLO script : ${SOLO}"
  runScript $SOLO
  source ${MY_DIR}/scripts/finished.sh
fi


# For each command line argument, try executing the corresponding script
echo "Running opt-in : $var"
for var in "$@"
do
    echo "Running opt-in : $var"
    runScript $var
done


source ${MY_DIR}/scripts/finished.sh
