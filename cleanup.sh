#!/bin/bash
set -e
# Clean all dependencies which are not mentioned in the brewfile

printf "\nCLEANING UP ALL OTHER DEPS"
brew bundle cleanup --force --file=./Brewfile

printf "\nUNINSTALLING BREWS"
brew list | xargs brew uninstall

printf "\n UNINSTALLING CASKS"
brew cask list | xargs brew cask uninstall

brew cleanup
