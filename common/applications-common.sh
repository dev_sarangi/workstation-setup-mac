# All these applications are independent, so if one
# fails to install, don't stop.
set +e

. ../scripts/util.sh

#Get the main workspace directory
echo -e "${YELLOW}"
read -p "Enter the absolute path for workspace directory" workspace_dir
echo -e "${NO_COLOR}"

#Create the workspace directory
if [ ! -d "${workspace_dir}" ]; then
    echo -e "Not found : ${workspace_dir}"
    read -p "Create (y/n) ?" create_dir
    if [[ "${create_dir}" == "y"]];then
        mkdir "${workspace_dir}"
    fi
    
fi

echo
echo -e "${YELLOW} Installing applications ${NO_COLOR}"
brew bundle install --file ../BrewFile


