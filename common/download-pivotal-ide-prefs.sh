#!/bin/bash

#Check if ide-prefs are installed


pushd ~/dswork
if [ ! -d ~/dswork/pivotal_ide_prefs ]; then
    echo
    echo "Downloading Pivotal IDE preferences"
    git clone https://github.com/pivotal/pivotal_ide_prefs.git
fi
popd

pushd ~/dswork/pivotal_ide_prefs/cli
./bin/ide_prefs install --ide=intellij
popd

set -e
